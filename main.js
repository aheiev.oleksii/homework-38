const products = [
    {
        category: 'Phones',
        products: [
            {
                name: 'Iphone 15',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1400,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Samsung Galaxy 30',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1400,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Nokia 1100',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 140,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
        ],
    },
    {
        category: 'Computers',
        products: [
            {
                name: 'Mac pro',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 2000,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Mac Air',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1400,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'ASUS',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1200,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
        ],
    },
    {
        category: "TV's",
        products: [
            {
                name: 'Samsung',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 700,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'LG',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1000,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Philips',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 1500,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
        ],
    },
    {
        category: 'Home appliances',
        products: [
            {
                name: 'Dishwasher',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 500,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Microwave',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 300,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Washing machine',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 700,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
        ],
    },
    {
        category: 'Tools',
        products: [
            {
                name: 'Tool set',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 2000,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Generator',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 3000,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
            {
                name: 'Hammer',
                image: 'https://picsum.photos/500/500',
                link: 'https://link',
                price: 30,
                descr: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Delectus eaque distinctio harum ea voluptatibus temporibus sed neque vero dolorum. Itaque, consectetur. Accusantium possimus, a quo voluptates consequatur eius! Consequuntur magnam qui tempora corporis, cumque quis, adipisci eveniet vel quidem voluptatum ratione odit consequatur ad unde nam quos quas eos culpa?',
            },
        ],
    },
];

const navList = document.querySelector('.nav-bar__categories');
const navItems = document.querySelector('.nav-bar__items');
const ul = document.createElement('ul');
const description = document.querySelector('.descr');
description.classList.add('d-none');
navItems.classList.add('d-none');
ul.classList.add('nav-bar__list');

function renderList(category) {
    category.forEach(link => {
        const li = document.createElement('li');
        const a = document.createElement('a');
        li.classList.add('nav-bar__link');

        li.addEventListener('click', e => {
            e.preventDefault();
        });

        li.addEventListener('click', () => {
            navItems.classList.toggle('d-none');
        });

        a.innerText = link.category;
        a.id = link.category;

        link.products.forEach(e => {
            a.href = e.link;
        });

        li.append(a);
        ul.append(li);
    });

    navList.append(ul);
}

renderList(products);

function renderItems(products) {
    products.forEach(card => {
        card.products.forEach(e => {
            const productCard = document.createElement('div');
            const img = document.createElement('img');
            const title = document.createElement('h3');
            const price = document.createElement('h4');
            productCard.classList.add('nav-bar__card');

            productCard.addEventListener('click', () => {
                description.classList.toggle('d-none');
                renderDesrcCard(e);
            });

            productCard.setAttribute('data-filter', `${card.category}`);
            img.src = e.image;
            img.alt = e.name;
            title.innerText = e.name;
            price.innerText = e.price + '$';

            productCard.append(img);
            productCard.append(title);
            productCard.append(price);
            navItems.append(productCard);
        });
    });
}
renderItems(products);

function renderDesrcCard(value) {
    description.innerHTML = '';

    const descrWrapp = document.createElement('div');
    const img = document.createElement('img');
    const name = document.createElement('h4');
    const price = document.createElement('h3');
    const descr = document.createElement('p');
    const btn = document.createElement('button');

    btn.addEventListener('click', () => {
        alert('Product purchased');
        navItems.innerHTML = '';
        description.innerHTML = '';
        document.location.reload();
    });

    img.src = value.image;
    img.alt = value.name;
    name.innerText = value.name;
    price.innerText = value.price + '$';
    descr.innerText = value.descr;
    btn.innerText = 'Order';

    descrWrapp.append(img);
    descrWrapp.append(name);
    descrWrapp.append(price);
    descrWrapp.append(descr);
    descrWrapp.append(btn);
    description.append(descrWrapp);
}

const filterLinks = document.querySelector('.nav-bar__list');
const galeryCards = document.querySelectorAll('.nav-bar__card');

filterLinks.addEventListener('click', e => {
    if (e.target.id) {
        e.target.classList.add('active');
        description.classList.add('d-none');

        galeryCards.forEach(card => {
            const dataFilter = card.getAttribute('data-filter');

            if (dataFilter === e.target.id) {
                card.classList.remove('hide');
                card.classList.add('show');
            } else {
                card.classList.remove('show');
                card.classList.add('hide');
            }
        });
    }
});
